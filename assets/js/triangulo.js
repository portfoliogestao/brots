// Função usada pra encapsular as rotinas de criação do
// gráfico, a partir de um objeto estruturado 
// padronizadamente.
function createTriangleChart($container, data) {
  // Crio um elemento table com a classe "triangle-chart",
  // que, nesse caso, é a estrutura base do gráfico. Além
  // disso, aproveito para declarar as variáveis a serem
  // usadas no escopo da função logo na sequência.
  var $triangleChart = $('<table class="triangle-chart">'),
  	// O cabeçalho é inicializado como uma linha da tabela
    // e com uma célula pra preencher espaço, de modo que 
    // possamos colocar uma legenda na primeira célula da
    // segunda linha.
  	$headerRow = $('<tr>').append($('<td>')),
    $lastRow = null,
  	$lastCol = null,
    lastRowValue = null,
  	lastColValue = null;
    
  // Função para centralizar a criação de células com legenda
  // que indiquem informações de cabeçalho.
  function appendHeaderCell($container, text) {
  	var $cell = $('<td class="header">').text(text);
    $container.append($cell);
  }
    
  // Para facilitar, primeiro montamos a linha de cabeçalho,
  // iterando pelos elementos até que o valor da linha seja
  // outro.
	for (var i = 1; i <= data.length; i++) {
    // Vamos adicionando os valores das colunas em uma nova
    // célula da tabela.
    appendHeaderCell($headerRow, data[i-1].col);
    
  	// Quando chegarmos em uma diferença nos valores,
    // encerramos a montagem do cabeçalho.
  	if (data[i].row !== data[i-1].row) {
    	break;
		}
  }
  
  // Inserimos o cabeçalho na tabela do nosso gráfico.
  $triangleChart.append($headerRow);
  
  // Iteramos pelos elementos do nosso array de dados, de
  // modo que a lógica aqui construa os elementos dentro da
  // nossa tabela, conforme layout que queremos obter.
	for (var i = 0; i < data.length; i++) {
  	var cellData = data[i];

		// Se a última linha não contiver o mesmo valor da atual,
    // significa que temos que inserir uma nova linha.
    if (lastRowValue !== cellData.row) {
    	// Se a última linha não for vazia, temos que inserí-la
      // na tabela que temos até então.
    	if ($lastRow !== null) $triangleChart.append($lastRow);
      
      // Indicamos a necessidade de uma nova linha limpando
      // essa variável (conforme lógica da próxima condição).
    	$lastRow = null;
    }
    
    // Se não tivermos uma última linha definida, sabemos que
    // devemos criar uma nova linha.
    if ($lastRow === null) {
    	// Criamos uma nova linha para a tabela.
    	$lastRow = $('<tr>');
      
      // Pelo número de linhas dentro da tabela mãe, podemos
      // saber quantas células devemos preencher no início 
      // (para dar o espaço necessário e formar nosso triângulo).
    	var cellsToFill = $('tr', $triangleChart).length - 1;
      console.log(cellsToFill);
      while (cellsToFill--) {
      	$lastRow.append('<td>');
      }
      
    	appendHeaderCell($lastRow, cellData.row);
    }
    
    $lastRow.append($('<td class="value">').text(cellData.value));
    
    lastRowValue = cellData.row;
  }
  
  // Como a nossa lógica depende de um controle baseado na 
  // diferença do valor última linha para a atual, sempre
  // a última linha da tabela ficará no nosso "buffer", uma
  // vez que o laço de iterações é finalizado pela condição
  // principal (que é ter iterado por todos os elementos).
  // Por conta disso, agora temos de forçar a inserção da 
  // última linha na tabela.
  $triangleChart.append($lastRow);
  
  // Por fim, limpamos nosso container, para garantir
  // que quaisquer gráficos anteriormente presentes 
  // sejam apagados (evitando duplicidade) e inserimos
  // o novo gráfico gerado nessa função.
  $container.empty().append($triangleChart);
}

// Essa é a representação dos dados do gráfico, que 
// pode vir via Ajax, por exemplo, para alimentar 
// essas rotinas. É importante dizer que a função
// aqui desenvolvida requer que os dados estejam
// ordenados dessa mesma forma, respeitando a 
// seguinte orientação: esquerda -> direita, 
// cima -> baixo.
var data = [
	{"row":"1","col":"1","value":"271"},
{"row":"1","col":"2","value":"271"},
{"row":"1","col":"3","value":"271"},
{"row":"1","col":"4","value":"168"},
{"row":"1","col":"5","value":"143"},
{"row":"1","col":"6","value":"112"},
{"row":"1","col":"7","value":"81"},
{"row":"1","col":"8","value":"64"},
{"row":"1","col":"9","value":"56"},
{"row":"1","col":"10","value":"48"},
{"row":"1","col":"11","value":"40"},
{"row":"1","col":"12","value":"36"},
{"row":"1","col":"13","value":"33"},
{"row":"1","col":"14","value":"26"},
{"row":"1","col":"15","value":"25"},
{"row":"1","col":"16","value":"24"},
{"row":"2","col":"2","value":"78"},
{"row":"2","col":"3","value":"78"},
{"row":"2","col":"4","value":"78"},
{"row":"2","col":"5","value":"39"},
{"row":"2","col":"6","value":"35"},
{"row":"2","col":"7","value":"25"},
{"row":"2","col":"8","value":"20"},
{"row":"2","col":"9","value":"16"},
{"row":"2","col":"10","value":"11"},
{"row":"2","col":"11","value":"10"},
{"row":"2","col":"12","value":"8"},
{"row":"2","col":"13","value":"8"},
{"row":"2","col":"14","value":"8"},
{"row":"2","col":"15","value":"7"},
{"row":"2","col":"16","value":"5"},
{"row":"3","col":"3","value":"91"},
{"row":"3","col":"4","value":"91"},
{"row":"3","col":"5","value":"91"},
{"row":"3","col":"6","value":"55"},
{"row":"3","col":"7","value":"35"},
{"row":"3","col":"8","value":"26"},
{"row":"3","col":"9","value":"21"},
{"row":"3","col":"10","value":"14"},
{"row":"3","col":"11","value":"9"},
{"row":"3","col":"12","value":"5"},
{"row":"3","col":"13","value":"4"},
{"row":"3","col":"14","value":"2"},
{"row":"3","col":"15","value":"2"},
{"row":"3","col":"16","value":"2"},
{"row":"4","col":"4","value":"100"},
{"row":"4","col":"5","value":"100"},
{"row":"4","col":"6","value":"100"},
{"row":"4","col":"7","value":"39"},
{"row":"4","col":"8","value":"26"},
{"row":"4","col":"9","value":"17"},
{"row":"4","col":"10","value":"13"},
{"row":"4","col":"11","value":"11"},
{"row":"4","col":"12","value":"7"},
{"row":"4","col":"13","value":"5"},
{"row":"4","col":"14","value":"4"},
{"row":"4","col":"15","value":"3"},
{"row":"4","col":"16","value":"3"},
{"row":"5","col":"5","value":"95"},
{"row":"5","col":"6","value":"95"},
{"row":"5","col":"7","value":"95"},
{"row":"5","col":"8","value":"58"},
{"row":"5","col":"9","value":"34"},
{"row":"5","col":"10","value":"23"},
{"row":"5","col":"11","value":"20"},
{"row":"5","col":"12","value":"18"},
{"row":"5","col":"13","value":"16"},
{"row":"5","col":"14","value":"10"},
{"row":"5","col":"15","value":"9"},
{"row":"5","col":"16","value":"8"},
{"row":"6","col":"6","value":"76"},
{"row":"6","col":"7","value":"76"},
{"row":"6","col":"8","value":"76"},
{"row":"6","col":"9","value":"35"},
{"row":"6","col":"10","value":"24"},
{"row":"6","col":"11","value":"18"},
{"row":"6","col":"12","value":"13"},
{"row":"6","col":"13","value":"11"},
{"row":"6","col":"14","value":"7"},
{"row":"6","col":"15","value":"5"},
{"row":"6","col":"16","value":"5"},
{"row":"7","col":"7","value":"112"},
{"row":"7","col":"8","value":"112"},
{"row":"7","col":"9","value":"112"},
{"row":"7","col":"10","value":"49"},
{"row":"7","col":"11","value":"32"},
{"row":"7","col":"12","value":"22"},
{"row":"7","col":"13","value":"18"},
{"row":"7","col":"14","value":"15"},
{"row":"7","col":"15","value":"14"},
{"row":"7","col":"16","value":"11"},
{"row":"8","col":"8","value":"90"},
{"row":"8","col":"9","value":"90"},
{"row":"8","col":"10","value":"90"},
{"row":"8","col":"11","value":"47"},
{"row":"8","col":"12","value":"28"},
{"row":"8","col":"13","value":"20"},
{"row":"8","col":"14","value":"14"},
{"row":"8","col":"15","value":"11"},
{"row":"8","col":"16","value":"8"},
{"row":"9","col":"9","value":"120"},
{"row":"9","col":"10","value":"120"},
{"row":"9","col":"11","value":"120"},
{"row":"9","col":"12","value":"48"},
{"row":"9","col":"13","value":"34"},
{"row":"9","col":"14","value":"22"},
{"row":"9","col":"15","value":"17"},
{"row":"9","col":"16","value":"13"},
{"row":"10","col":"10","value":"50"},
{"row":"10","col":"11","value":"50"},
{"row":"10","col":"12","value":"50"},
{"row":"10","col":"13","value":"25"},
{"row":"10","col":"14","value":"20"},
{"row":"10","col":"15","value":"14"},
{"row":"10","col":"16","value":"9"},
{"row":"11","col":"11","value":"69"},
{"row":"11","col":"12","value":"69"},
{"row":"11","col":"13","value":"69"},
{"row":"11","col":"14","value":"26"},
{"row":"11","col":"15","value":"18"},
{"row":"11","col":"16","value":"11"},
{"row":"12","col":"12","value":"48"},
{"row":"12","col":"13","value":"48"},
{"row":"12","col":"14","value":"48"},
{"row":"12","col":"15","value":"25"},
{"row":"12","col":"16","value":"14"},
{"row":"13","col":"13","value":"41"},
{"row":"13","col":"14","value":"41"},
{"row":"13","col":"15","value":"41"},
{"row":"13","col":"16","value":"23"},
{"row":"14","col":"14","value":"48"},
{"row":"14","col":"15","value":"48"},
{"row":"14","col":"16","value":"48"},
{"row":"15","col":"15","value":"63"},
{"row":"15","col":"16","value":"63"},
{"row":"16","col":"16","value":"57"}
];

// Dispara a função para montar o gráfico assim que
// o script carregar (poderia ser em um clique de
// botão, por exemplo, ou em um intervalo de tempo
// no caso de se querer atualizar constantemente).
// O primeiro parâmetro é o elemento que será o container
// e o segundo a estrutura de dados a ser utilizada, que
// deverá estar nesse formato pré-definido.
createTriangleChart($('#triangle-chart-container'), data);

// Notas gerais, dicas e boas práticas:
// - Por convenção, quando uma estrutura do jQuery é 
//   atribuída a uma variável, a nomeamos com um cifrão
//   ("$") na frente, de modo a facilitar o entendimento
//   e a rápida compreensão do código lido;
// - Costumo utilizar apenas aspas simples em declarações
//   de strings no JavaScript, por um motivo bem simples:
//   facilita quando precisamos declarar qualquer tags e,
//   nelas, usar aspas duplas, evitando que seja necessário
//   usar o código de escape (\");
// - Geralmente, quando queremos construir tabelas a partir
//   de dados estruturados, costumamos primeiro facilitar
//   a construção da tabela já alterando a estrutura dos
//   dados (que seria o que eu faria usando Angular, por ele
//   ter uma facilidade de manipular o HTML diretamente e,
//   por consequência, necessitar que essa informação chegue
//   mais pronta); nesse exemplo, no entanto, preferi manter
//   de maneira simples, mais tradicional, para ficar mais
//   didático;
// - Lembro que em vários ambientes que desenvolvi sistemas
//   tínhamos a ideia de que comentários no código são
//   imprescindíveis para o entendimento da lógica; no entanto
//   com o passar dos anos e maior experiência, vi que isso
//   de fato é mais um preciosismo do que uma verdade e,
//   inclusive, muitos comentários (como aqui) atrapalham;
//   só estou fazendo dessa forma porque esse exemplo tem o
//   objetivo de explicar mesmo; na realidade, código que
//   precisa de muitos comentários, no geral, é porque não
//   está bem escrito e/ou não segue convenções, boas práticas
//   e/ou padrões de desenvolvimento.
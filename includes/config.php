<?php
    // Buffer da saida
    ob_start();

    // Inicia sessao
    session_start();

    // Configuracao data/hora
    date_default_timezone_set('America/Sao_Paulo');

    // Credenciais do banco de dados
    define('DBHOST','sql130.main-hosting.eu');
    // define('DBHOST','mysql.hostinger.com.br');
    define('DBUSER','u680651490_brots');
    define('DBPASS','Brots@2018');
    define('DBNAME','u680651490_brots');

    // Endereco da aplicacao
    // define('DIR','localhost/login/');
    define('DIR','www.marketinganalytics.esy.es');
    define('SITEEMAIL','joao.brito.brots@gmail.com');

    try {
        // Cria conexao PDO
        $db = new PDO("mysql:host=".DBHOST.";charset=utf8;dbname=".DBNAME, DBUSER, DBPASS);
        //$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);//Suggested to uncomment on production websites
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//Suggested to comment on production websites
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    } catch(PDOException $e) {
        // Mostra excecao
        echo '<p class="bg-danger">'.$e->getMessage().'</p>';
        exit;
    }

    // Classes para usuario, conexao e recuperacao de senha
    include('classes/user.php');
    include('classes/phpmailer/mail.php');
    $user = new User($db);
?>
